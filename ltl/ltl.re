type option('a) =
  | None
  | Some('a);

module Shipment = {
  type t = {
    label: string,
    weight: int,
  };

  let create = () => {label: "", weight: 0};

  let to_string = shipment =>
    Printf.sprintf("Shipment %s %d\n", shipment.label, shipment.weight);

  let to_string = (shipments: list(t)) => {
    let rec reduce = (shipments, acc) =>
      switch (shipments) {
      | [h, ...t] => reduce(t, acc ++ to_string(h))
      | [] => acc
      };
    reduce(shipments, "");
  };
};

module Shipments = {
  type t = list(Shipment.t);

  let order = (s1: Shipment.t, s2: Shipment.t) =>
    compare(s1.weight, s2.weight) * (-1);

  let sort = (shipments: t) => List.sort(order, shipments);
};

module Truck = {
  type t = {
    label: string,
    max_weight: int,
    current: int,
    shipments: list(Shipment.t),
  };

  let create = () => {label: "", current: 0, max_weight: 0, shipments: []};

  let add_shipment = (truck: t, shipment: Shipment.t) => {
    ...truck,
    shipments: [shipment, ...truck.shipments],
    current: truck.current + shipment.weight,
  };

  let to_string = t =>
    Printf.sprintf(
      "Truck %s  max_weight: %d current: %d shipments: \n%s\n",
      t.label,
      t.max_weight,
      t.current,
      Shipment.to_string(t.shipments),
    );

  let to_string = (shipments: list(t)) => {
    let rec reduce = (shipments, acc) =>
      switch (shipments) {
      | [h, ...t] => reduce(t, acc ++ to_string(h))

      | [] => acc
      };
    reduce(shipments, "");
  };
};

module Trucks = {
  type t = list(Truck.t);

  let order = (t1: Truck.t, t2: Truck.t) =>
    compare(t1.max_weight, t2.max_weight);

  let sort = (trucks: t) => List.sort(order, trucks);

  /* Insert/Update truck in list */
  let upsert = (trucks: t, truck: Truck.t) =>
    [truck, ...List.filter((x: Truck.t) => x.label != truck.label, trucks)]
    |> sort;

  /* Empty all shipments and current weight*/
  let reset = (trucks: t) =>
    List.map(
      truck => ({...truck, current: 0, shipments: []}: Truck.t),
      trucks,
    )
    |> sort;
};

let has_room = (shipment: Shipment.t, truck: Truck.t) =>
  shipment.weight + truck.current <= truck.max_weight;

let rec add_to_truck = (shipment: Shipment.t, trucks: Trucks.t) =>
  switch (trucks) {
  | [th, ..._tt] when has_room(shipment, th) =>
    Some(Truck.add_shipment(th, shipment))

  | [_th, ...tt] => add_to_truck(shipment, tt)

  | [] => None
  };

let rec match =
        (~acc: Shipments.t=[], shipments: Shipments.t, trucks: Trucks.t) =>
  switch (shipments) {
  | [sh, ...st] =>
    switch (add_to_truck(sh, trucks)) {
    | Some(truck) => match(~acc, st, Trucks.upsert(trucks, truck))

    | None => match(~acc=[sh, ...acc], st, trucks)
    }
  | [] => (acc, trucks)
  };

let rec ship =
        (~acc: list(Trucks.t)=[], shipments: Shipments.t, trucks: Trucks.t) =>
  switch (shipments) {
  | [] => acc
  | shipments =>
    let (remaining_shipments, trucks) = match(shipments, trucks);
    ship(~acc=[trucks, ...acc], remaining_shipments, Trucks.reset(trucks));
  };

/* Test Cases */
let%test _ =
  has_room(
    {...Shipment.create(), weight: 100},
    {...Truck.create(), current: 100},
  )
  == false;

let%test _ =
  has_room(
    {...Shipment.create(), weight: 10},
    {...Truck.create(), current: 0, max_weight: 100},
  )
  == true;

let%test _ = {
  let t =
    add_to_truck(
      {...Shipment.create(), weight: 10},
      [{...Truck.create(), current: 0, max_weight: 100}],
    );

  switch (t) {
  | Some(_t) => true
  | None => false
  };
};

let%test _ = {
  let shipment = {...Shipment.create(), weight: 10};
  let t =
    add_to_truck(
      shipment,
      [{...Truck.create(), current: 0, max_weight: 100}],
    );

  switch (t) {
  | Some(truck) =>
    List.hd(truck.shipments) == shipment && truck.current == shipment.weight
  | None => false
  };
};

let%test _ = {
  let t =
    match(
      [
        {...Shipment.create(), weight: 10},
        {...Shipment.create(), weight: 20},
      ],
      [{...Truck.create(), current: 0, max_weight: 100}],
    );

  switch (t) {
  | ([], [truck]) =>
    List.length(truck.shipments) == 2 && truck.current == 30
  | _ => false
  };
};

let%test _ = {
  let t =
    ship(
      [
        {...Shipment.create(), weight: 10},
        {...Shipment.create(), weight: 20},
      ],
      [{...Truck.create(), current: 0, max_weight: 100}],
    );

  switch (t) {
  | [[truck]] => List.length(truck.shipments) == 2 && truck.current == 30
  | _ => false
  };
};