open Ltl;
let () = {
  let shipments: list(Shipment.t) =
    Shipments.sort([
      {label: "001", weight: 16000},
      {label: "002", weight: 42000},
      {label: "003", weight: 8000},
      {label: "004", weight: 12000},
      {label: "005", weight: 38000},
      {label: "006", weight: 1200},
      {label: "007", weight: 1000},
      {label: "009", weight: 18000},
      {label: "010", weight: 28000},
      {label: "011", weight: 7500},
      {label: "012", weight: 17000},
      {label: "013", weight: 37000},
    ]);

  let trucks =
    Trucks.sort([
      {label: "001", max_weight: 44000, current: 0, shipments: []},
      {label: "002", max_weight: 42000, current: 0, shipments: []},
      {label: "003", max_weight: 20000, current: 0, shipments: []},
      {label: "004", max_weight: 24000, current: 0, shipments: []},
    ]);

  let all_trucks_with_shipments = ship(shipments, trucks) |> List.rev;
  List.iteri(
    (i, trucks) => {
      Printf.printf("Shipment %d:\n", i + 1);
      Truck.to_string(trucks) |> print_endline;
    },
    all_trucks_with_shipments,
  );
};