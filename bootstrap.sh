#!/bin/bash
sudo apt update && apt install -y build-essential m4 git mercurial darcs nodejs curl unzip bubblewrap rsync
curl -sL https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh > /tmp/opam.sh
echo -ne '\n' | sh /tmp/opam.sh --fresh --no-backup

yes yes | opam init -c 4.06.1
opam install --yes dune ppx_expect reason ppx_inline_test