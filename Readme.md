## Process

I initially started this challenge with Elixir but decided to write it in ReasonML (compiling to native executable). 
ReasonML/Ocaml is something I've been wanting to learn but never had a project with a small enough scope in mind. 

The code included attempts to solve the less than truck load assignment. Sorting the shipments and trucks in ascending and descending order based on weight. Then matching shipments with the first available truck. 

Once a match happens the trucks are sorted back into descending order. The program continues until all shipments are accounted for.

## Running with Vagrant
```
Host:
vagrant up
vagrant ssh

Vagrant:
sudo su
cd /vagrant
eval $(opam env)
dune build bin/cli.exe && ./_build/default/bin/cli.exe
dune runtest
```

## Run locally (probably faster)

* install opam: https://opam.ocaml.org/doc/Install.html
* Provision Ocaml 4.06.1 (ReasonML/Bucklescript locked to 4.06 for now): `opam init -c 4.06.1`
* Install dependencies: `opam install --yes dune ppx_expect reason ppx_inline_test`
* From project root folder:
  * `dune runtest`
  * `dune exec bin/cli.exe`